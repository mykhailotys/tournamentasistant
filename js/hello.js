var app = angular.module('competition', ['ngRoute'])

app.config(['$routeProvider', '$locationProvider',
  function($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'competitions.html',
        controller: 'competitionController'
      })
      .when('/competitionView', {
        templateUrl: 'competitionView.html',
        controller: 'competitionController'
      })
      
   // $locationProvider.html5Mode(true);
}]);

app.controller('competitionsController', function ($scope, $http, $location) {
	// $location.path('competitions.html')
	$http.get('/competition/competitions').success(function(data) {
		$scope.competitions = data;
	}); 
	$scope.openCompetition = function(i){
		console.log(i)
		$location.path('/competitionView')
	};
});

app.controller('competitionController', function ($scope, $http) {
	
});

app.controller('MainCtrl', ['$route', '$routeParams', '$location',
  function($route, $routeParams, $location) {
  	console.log('MainCtrl')
    this.$route = $route;
    this.$location = $location;
    this.$routeParams = $routeParams;
}])